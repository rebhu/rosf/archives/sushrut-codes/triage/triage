# Triage
Triage patients digitally from all over India and the world, in general.

## Purpose
This repository is a single point for all development and deployment needs of ROSF Sushrut Codes Triage.

## Chat
If you have suggestions that you would like to share then join either [Element](https://matrix.to/#/!xfABEliXqOZqiOpYOs:matrix.org?via=matrix.org&via=t2bot.io&via=gitter.im) or [Discord](https://discord.gg/SK5sPPdf). Messages are relayed from one to the other. We are live on Element.